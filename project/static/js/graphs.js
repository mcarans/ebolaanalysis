queue()
    .defer(d3.json, "/eboladata/projects")
    .await(makeGraphs);

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function makeGraphs(error, projectsJson) {
	
	//Fix ebola data dates
	var EboladataProjects = projectsJson;
	var dateFormat = d3.time.format("%m/%d/%Y %H:%M:%S");
	EboladataProjects.forEach(function(d) {
		d["esdate"] = dateFormat.parse(d["esdate"]);
		d["esdate"].setDate(1);
	});

	//Create a Crossfilter instance
	var ndx = crossfilter(EboladataProjects);

	//Define Dimensions
	var dateDim = ndx.dimension(function(d) { return d["esdate"]; });
	var respondentDim = ndx.dimension(function(d) { return d["respid"]; });
	var noCashFoodDim = ndx.dimension(function(d) { return [d["nocashjuly"], d["nofoodjuly"]]});
	var noTreatmentDim = ndx.dimension(function(d) { return d["notreatmentj26"] ? d["notreatmentj26"].substr(2, d["notreatmentj26"].length).replace("-", "") : "Ignore"; });
	var wardDim = ndx.dimension(function(d) { return d["ward"].substr(5, d["ward"].length).replace("-", ""); });
	var genderDim = ndx.dimension(function(d) { return d["gender"].substr(2, d["gender"].length); });
	var educDim = ndx.dimension(function(d) { return d["educ"] ? d["educ"].substr(2, d["educ"].length).replace("-", "") : "No Response";  });	
	var religionDim = ndx.dimension(function(d) { return d["religion"] ? d["religion"].substr(2, d["religion"].length).replace("-", "") : "NA"; });	
	var religionMaleDim = ndx.dimension(function(d) { return d["gender"] == "1-Male" ? (d["religion"] ? d["religion"].substr(2, d["religion"].length).replace("-", "") : "NA") : "Ignore"; });	
	var religionFemaleDim = ndx.dimension(function(d) { return d["gender"] == "2-Female" ? (d["religion"] ? d["religion"].substr(2, d["religion"].length).replace("-", "") : "NA") : "Ignore"; });	
	var leavemrDim = ndx.dimension(function(d) { return d["leavemonr2wwhere"] ? d["leavemonr2wwhere"].substr(2, d["leavemonr2wwhere"].length).replace("-", "") : "Ignore"; });
	var income7dDim = ndx.dimension(function(d) { return d["income7d"]; });
	var skipmealsDim = ndx.dimension(function(d) { return d["skipmeals"]; });
	var childmealsDim = ndx.dimension(function(d) { return d["childmealsnormal"] != 99 ? d["childmealsnormal"] - d["childmeals"]: "Ignore"; });

	//Calculate metrics
	var leavemrGroup = leavemrDim.group();
	var filteredleavemrGroup = remove_ignore(leavemrGroup)
	var wardGroup = wardDim.group();
	var skipmealsGroup = skipmealsDim.group().reduceCount();
	var noCashFoodGroup = noCashFoodDim.group().reduceCount();
	var noTreatmentGroup = noTreatmentDim.group();
	var filterednoTreatmentGroup = remove_ignore(noTreatmentGroup);
	var numRespondentsByGender = genderDim.group();
	var numRespondentsByEducation = educDim.group();
	var numRespondentsByReligion = religionDim.group();
	var numMaleRespondentsByReligion = religionMaleDim.group();
	var numFemaleRespondentsByReligion = religionFemaleDim.group();
	var filteredMaleRespondentsByReligion = remove_ignore(numMaleRespondentsByReligion)
	var filteredFemaleRespondentsByReligion = remove_ignore(numFemaleRespondentsByReligion)
	
	// Calculates average income before and after Ebola and average household size per ward
    var wardDimensionGroup = wardDim.group().reduce(
        //add
        function(p,v){
			if (v.income7dunit == "1-LD" && isNumeric(v.income7d)) {
				++p.income7d_count;
				p.income7d_sum += v.income7d / 84.66  // convert to USD
			}
			else if (v.income7dunit == "2-USD" && isNumeric(v.income7d)) {
				++p.income7d_count;
				p.income7d_sum += v.income7d
			}
			if (v.income7dnormalunit == "1-LD" && isNumeric(v.income7dnormal)) {
				++p.income7dnormal_count;
				p.income7dnormal_sum += v.income7dnormal / 84.66
			}
			else if (v.income7dnormalunit == "2-USD" && isNumeric(v.income7dnormal)) {
				++p.income7dnormal_count;
				p.income7dnormal_sum += v.income7dnormal
			}
			if(isNumeric(v.hhsize)) {
				++p.hhsize_count;
				p.hhsize_sum += v.hhsize;
			}
			if(p.hhsize_count == 0) {
				p.hhsize_avg = 0;
			} else {
				p.hhsize_avg = p.hhsize_sum / p.hhsize_count;
			}
			if(p.income7d_count == 0) {
				p.income7d_avg = 0;
			} else {
				p.income7d_avg = p.income7d_sum / p.income7d_count;
			}
			if(p.income7dnormal_count == 0) {
				p.income7dnormal_avg = 0;
			} else {
				p.income7dnormal_avg = p.income7dnormal_sum / p.income7dnormal_count;
			}
            return p;
        },
        //remove
        function(p,v){
			if (v.income7dunit == "1-LD" && isNumeric(v.income7d)) {
				--p.income7d_count;
				p.income7d_sum -= v.income7d / 84.66
			}
			else if (v.income7dunit == "2-USD" && isNumeric(v.income7d)) {
				--p.income7d_count;
				p.income7d_sum -= v.income7d
			}
			if (v.income7dnormalunit == "1-LD" && isNumeric(v.income7dnormal)) {
				--p.income7dnormal_count;
				p.income7dnormal_sum -= v.income7dnormal / 84.66
			}
			else if (v.income7dnormalunit == "2-USD" && isNumeric(v.income7dnormal)) {
				--p.income7dnormal_count;
				p.income7dnormal_sum -= v.income7dnormal
			}
			if(isNumeric(v.hhsize)) {
				--p.hhsize_count;
				p.hhsize_sum -= v.hhsize;
			}
			if(p.hhsize_count == 0) {
				p.hhsize_avg = 0;
			} else {
				p.hhsize_avg = p.hhsize_sum / p.hhsize_count;
			}
			if(p.income7d_count == 0) {
				p.income7d_avg = 0;
			} else {
				p.income7d_avg = p.income7d_sum / p.income7d_count;
			}
			if(p.income7dnormal_count == 0) {
				p.income7dnormal_avg = 0;
			} else {
				p.income7dnormal_avg = p.income7dnormal_sum / p.income7dnormal_count;
			}
            return p;
        },
        //init
        function(p,v){
            return {hhsize_count:0, income7d_count:0, income7dnormal_count:0, hhsize_sum: 0, hhsize_avg: 0, income7d_sum: 0, income7dnormal_sum: 0, income7d_avg: 0, income7dnormal_avg: 0};
        }
    );
 

    //Charts
	var treatmentChart = dc.pieChart("#treatment-pie-chart");
	var educChart = dc.rowChart("#education-row-chart");
	var religionChart = dc.barChart("#religion-row-chart");
	var bubbleChart = dc.bubbleChart("#income-bubble-graph");
	var rowChart = dc.rowChart("#leavemr-row-chart");
	var heatMap = dc.heatMap("#cashfood-heatmap-chart");
	var dataTable = dc.dataTable("#data-table");

	bubbleChart.width(650)
		.height(300)
		.dimension(wardDim)
		.group(wardDimensionGroup)
		.transitionDuration(1500)
		.colorDomain([-12000, 12000])
	 
		.x(d3.scale.linear().domain([40, 100]))
		.y(d3.scale.linear().domain([0, 100]))
		.r(d3.scale.linear().domain([7, 20]))
		.keyAccessor(function (p) {
			return p.value.income7dnormal_avg;
		})
		.valueAccessor(function (p) {
			return p.value.income7d_avg;
		})
		.radiusValueAccessor(function (p) {
			return p.value.hhsize_avg;
		})
		.transitionDuration(1500)
		.elasticY(true)
		.yAxisPadding(1)
		.xAxisPadding(1)
		.xAxisLabel("Income before Ebola (USD/week)")
		.yAxisLabel("Income after Ebola (USD/week)")
		.label(function (p) {
			return p.key;
			})
		.renderLabel(true);
		
	var frequencyDict = new Array();
	frequencyDict[0] = 'Never';
	frequencyDict[1] = '1-2';
	frequencyDict[2] = 'Several';
	frequencyDict[3] = 'Many';
	frequencyDict[4] = 'Always';
	frequencyDict[5] = 'NA';
	heatMap
    .width(45 * 5 + 80)
    .height(45 * 5 + 30)
	.margins({ top: 0, right: 0, bottom: 40, left: 40 })
    .dimension(noCashFoodDim)
    .group(noCashFoodGroup)
    .keyAccessor(function(d) { return convertToNum(d.key[0]); })
    .valueAccessor(function(d) { return convertToNum(d.key[1]); })
    .colorAccessor(function(d) { return +d.value; })
    .title(function(d) {
        return "Cash:   " + d.key[0] + "\n" +
               "Food:  " + d.key[1] + "\n" +
               "Number: " + d.value;})
	.rowsLabel(function(d) { return frequencyDict[d];  })
	.colsLabel(function(d) { return frequencyDict[d];  })
    .colors(["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"])
    .calculateColorDomain();

	treatmentChart
		.width(200)
		.height(200)
        .dimension(noTreatmentDim)
        .group(filterednoTreatmentGroup)
		.innerRadius(10)
		.renderLabel(false)
		.legend(dc.legend().x(210).y(50).gap(5));

	educChart
		.width(300)
		.height(250)
		.elasticX(true)
        .dimension(educDim)
        .group(numRespondentsByEducation)		
        .xAxis().ticks(4);

	religionChart
		.width(300)
		.height(250)
        .dimension(religionDim)
        .group(filteredMaleRespondentsByReligion, "Male")
		.stack(filteredFemaleRespondentsByReligion, "Female")
		.legend(dc.legend().x(150).y(10))
		.elasticY(true)
		.x(d3.scale.ordinal())
		.xUnits(dc.units.ordinal)		
        .xAxis().ticks(4);

	rowChart.width(340)
		.height(850)
		.dimension(leavemrDim)
		.group(filteredleavemrGroup)
		.elasticX(true)
		.renderLabel(true)
		.colorDomain([0, 0]);

	dataTable
        .width(400)
        .height(400)
        .size(4)
        .dimension(childmealsDim)
        .group(function (d) { return "<br>" })
        .columns([
			function(d) { return d.childmealsnormal; },
			function(d) { return d.childmeals; },
			function(d) { return d.skipmeals; }
		])
        .sortBy(function (d) { return d.childmealsnormal; })
        .order(d3.descending);
		
    dc.renderAll();

};

//Converts the 0-Never, 1-Once or Twice... to a number from 0 to 5
function convertToNum(str) {
	if (!str) {
		str = "9";
	}
	val = +str.substr(0, 1)
	if (val == 9) {
		val = 5;
	}
	return val
}

//Removes dimensions with "Ignore" from graphs
function remove_ignore(source_group) {
	return {
		all:function () {
			return source_group.all().filter(function(d) {
				return d.key != "Ignore";
			});
		}
	}
}
