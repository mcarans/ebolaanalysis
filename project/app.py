from flask import Flask
from flask import render_template
from pymongo import MongoClient
import json
from bson import json_util
from bson.json_util import dumps

app = Flask(__name__)

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DBS_NAME = 'eboladata'
COLLECTION_NAME = 'projects'
FIELDS = {'esdate': True, 'towncode': True, 'ward': True, 'respid': True, 'age': True, 'gender': True, 'hhsize': True, 'educ': True, 'religion': True, 'leavemonr2wwhere': True, 'under5': True, 'income7d': True, 'income7dunit': True, 'income7dnormal': True, 'income7dnormalunit': True, 'skipmeals': True, 'nofoodjuly': True, 'notreatmentj26': True, 'nocashjuly': True, 'childmeals': True, 'childmealsnormal': True, '_id': False}

@app.route("/")
def index():
    return render_template("index.html")

"""Connects to the MongoDB and exposes JSON on the app route above

Returns:
	 JSON of Mongo collection
"""
@app.route("/eboladata/projects")
def eboladata_projects():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DBS_NAME][COLLECTION_NAME]
    projects = collection.find(projection=FIELDS, limit=1000)
    json_projects = []
    for project in projects:
        json_projects.append(project)
    json_projects = json.dumps(json_projects, default=json_util.default)
    connection.close()
    return json_projects

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=True)