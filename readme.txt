Ebola Analysis is a dashboard created with DC.js, D3.js, Crossfilter, MongoDB, Python and Flask. It should work on Python 2 or 3.

Run Mongo: 
mongod

Import the data into mongodb:
mongoimport -d eboladata -c projects --type csv --file ebola-data-public.csv --headerline

Check data's there:
mongo
use eboladata
show collections
db.projects.findOne()

Set up virtualenv and install requirements:
Unix:
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt

Windows:
virtualenv venv
venv\Scripts\activate
pip install -r requirements.txt

The web server, Flask, runs on Python. To run it:
cd to the project folder
python app.py

The JSON of the imported fields can be seen in the web browser:
http://localhost:5000/eboladata/projects  : JSON of ebola data

The dashboard can be displayed in a web browser here:
http://localhost:5000/

It should look like the screenshot: WhatYouShouldSee.png
